##Client registration

### client id - 0oa2wmzoh3NgEXtSP5d7
### client secret - JDhrdc_FrRJWCxkaUlPnnPK0gDuMfY3EqiIUJxc6
### Redirect URI - http://localhost:8555/authorization-code/callback



1. Meta data url - https://dev-7858070.okta.com/oauth2/default/.well-known/oauth-authorization-server
2. Authorize endpoint - https://dev-7858070.okta.com/oauth2/default/v1/authorize"
3. Token endpoint - https://dev-7858070.okta.com/oauth2/default/v1/token


- Link
``` 
https://developer.okta.com/docs/guides/implement-grant-type/authcode/main/#flow-specifics
```
### Auth code grant flow
 1. User will be redirected to Auth server endpoint

```
 https://dev-7858070.okta.com/oauth2/default/v1/authorize?client_id=0oa2wmzoh3NgEXtSP5d7&response_type=code&scope=openid&redirect_uri=http%3A%2F%2Flocalhost%3A8555%2Fauthorization-code%2Fcallback&state=state-a7f62dae-519b-11ec-bf63-0242ac130002
 ```
 
# Response from Auth server -
``` 
http://localhost:8555/authorization-code/callback?code=vdHIfVeC3dbOdt12PVD3wdLM3LToyerjTW1gImochW0&state=state-a7f62dae-519b-11ec-bf63-0242ac130002
```
## Authorization code - no2M_QMJeOltS8IJy9DBxHgYpfgv8ZqZHep0gJcu1G8

### Exchange authorization code for ID token and Access token 

```
curl --location --request POST 'https://dev-7858070.okta.com/oauth2/default/v1/token' \
--header 'accept: application/json' \
--header 'authorization: Basic MG9hMndtem9oM05nRVh0U1A1ZDc6SkRocmRjX0ZyUkpXQ3hrYVVsUG5uUEswZ0R1TWZZM0VxaUlVSnhjNg==' \
--header 'content-type: application/x-www-form-urlencoded' \
--header 'Cookie: JSESSIONID=A0A8F8EE06BC997F6B5BF2E9D8B84EBC' \
--data-urlencode 'grant_type=authorization_code' \
--data-urlencode 'redirect_uri=http://localhost:8555/authorization-code/callback' \
--data-urlencode 'code=no2M_QMJeOltS8IJy9DBxHgYpfgv8ZqZHep0gJcu1G8'
```
