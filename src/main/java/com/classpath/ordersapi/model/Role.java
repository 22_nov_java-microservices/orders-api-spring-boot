package com.classpath.ordersapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="roles")
@Data
@NoArgsConstructor
@ToString(exclude = "users")
@EqualsAndHashCode(exclude = "users")
@Builder
@AllArgsConstructor
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String roleName;

    @ManyToMany
    @JsonIgnore
    @JoinTable(
              name = "user_roles",
              joinColumns = @JoinColumn(name="role_id"),
              inverseJoinColumns = @JoinColumn(name="user_id")
         )
    private Set<User> users = new HashSet<>();

    public Set<User> getUsers(){
        if(this.users == null){
            this.users = new HashSet<>();
        }
        return this.users;
    }
}