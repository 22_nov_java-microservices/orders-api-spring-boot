package com.classpath.ordersapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="project")
@NoArgsConstructor
@Data
@AllArgsConstructor
@Builder
@ToString(exclude = "employees")
@EqualsAndHashCode(exclude = "employees")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    private String location;

    @ManyToMany
        @JsonIgnore
        @JoinTable(
             name = "project_employees",
             joinColumns = @JoinColumn(name="project_id"),
             inverseJoinColumns = @JoinColumn(name="emp_id")
        )
    private Set<Employee> employees = new HashSet<>();

}