package com.classpath.ordersapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long orderId;

    @Min(value = 10000, message = "min order value should be 10k")
    @Max(value = 50000, message = "max order value can be 50k")
    private double price;

    @PastOrPresent(message = "Order date cannot be in future")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate date;

    @NotEmpty(message = "customer name cannot be empty")
    @Column(name = "customer_name", nullable = false)
    private String customerName;

    @Email(message = "not a valid email address")
    @Column(name = "email", nullable = false)
    private String customerEmail;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = EAGER)
    private Set<LineItem> lineItems;

    //scaffolding code
    public void addLineItem(LineItem lineItem){
        if ( lineItems == null){
            lineItems = new HashSet<>();
        }
        //lineItems should have been initialized correctly by this time
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }

    //scaffolding code for setting both sides of the relationships
    public void setLineItems(Set<LineItem> lineItems){
        lineItems.forEach(lineItem -> lineItem.setOrder(this));
        this.lineItems = lineItems;
    }
}