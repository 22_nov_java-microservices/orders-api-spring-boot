package com.classpath.ordersapi.config;

import com.classpath.ordersapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class DBHealthEndpoint implements HealthIndicator {

    private final OrderRepository orderRepository;

    @Override
    public Health health() {
        long count = this.orderRepository.count();
        if(count <= 0) {
            return Health.down().withDetail("DB Health", "DB status is down").build();
        }
        return Health.up().withDetail("DB Health", "DB status is up").build();
    }
}

@Component
class MessageBrokerHealthEndpoint implements  HealthIndicator{

    @Override
    public Health health() {
        return Health.up().withDetail("Kafka Health", "Kafka service is up").build();
    }
}

@Component
class PaymentGateway implements HealthIndicator {

    @Override
    public Health health() {
        return Health.up().withDetail("Payment Gateway", "Payment Gateway service is up").build();
    }
}