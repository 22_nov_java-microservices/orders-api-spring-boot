package com.classpath.ordersapi.config;

import com.classpath.ordersapi.model.*;
import com.classpath.ordersapi.model.User;
import com.classpath.ordersapi.repository.EmployeeRepository;
import com.classpath.ordersapi.repository.OrderRepository;
import com.classpath.ordersapi.repository.UserRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static java.util.stream.IntStream.range;

@Configuration
@RequiredArgsConstructor
@Profile("dev")
//This class will be loaded once all the beans are loaded and initialized and when the application is ready to server the requests
public class BootstrapAppConfig implements ApplicationListener<ApplicationReadyEvent> {

    private final OrderRepository orderRepository;

    private final EmployeeRepository employeeRepository;

    private final UserRepository userRepository;

    private final Faker faker = new Faker();

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        /*
         version -1
        for (int i = 0; i < 2000; i++) {
            Order or
            der = Order.builder().customerEmail("").customerName("").date(null).price(34).build();
            this.orderRepository.save(order);
        }*/
        //version - 2
        /*for (int i = 0; i < 2000; i++) {
            Order order = Order.builder()
                                .customerEmail(faker.internet().emailAddress().toLowerCase())
                                .customerName(faker.name().fullName())
                                .date(faker.date().past(5, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                                .price(faker.number().randomDouble(2, 35000, 50000)).build();
            this.orderRepository.save(order);
        }*/

        //version - 3
        range(1, 40)
                .forEach( (index) -> {
                        Order order = Order.builder()
                                .customerEmail(faker.internet().emailAddress().toLowerCase())
                                .customerName(faker.name().fullName())
                                .date(faker.date().past(16, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                                .price(faker.number().randomDouble(2, 35000, 45000))
                                .build();
                    //Set<LineItem> lineItems = new HashSet<>();
                        IntStream.range(1, 3).forEach((value) -> {
                            LineItem lineItem = LineItem.builder()
                                    .qty(faker.number().numberBetween(2,4))
                                    .unitPrice(faker.number().randomDouble(2, 1500, 3000))
                                    .name(faker.commerce().productName())
                                    .build();

                            order.addLineItem(lineItem);

                            //lineItems.add(lineItem);

                            //set both sides of relationship - should be managed by the client
                            //order.setLineItems(lineItems);
                            //lineItem.setOrder(order);

                        });
                        this.orderRepository.save(order);});

        //demonstrate the many-to-many relationship
        Employee employee1 = Employee.builder().email(faker.internet().emailAddress()).projects(new HashSet<>()).name(faker.name().fullName()).build();
        Employee employee2 = Employee.builder().email(faker.internet().emailAddress()).projects(new HashSet<>()).name(faker.name().fullName()).build();

        Set<Employee> employees = new HashSet<>();
        employees.add(employee1);
        employees.add(employee2);


        Project project1 = Project.builder().employees(new HashSet<>()).location(faker.address().city()).name(faker.company().name()).build();
        Project project2 = Project.builder().employees(new HashSet<>()).location(faker.address().city()).name(faker.company().name()).build();
        Project project3 = Project.builder().employees(new HashSet<>()).location(faker.address().city()).name(faker.company().name()).build();
        Project project4 = Project.builder().employees(new HashSet<>()).location(faker.address().city()).name(faker.company().name()).build();


        Set<Project> projects = new HashSet<>();
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        projects.add(project4);

        project1.getEmployees().add(employee1);
        project2.getEmployees().add(employee1);

        employee1.getProjects().add(project1);
        employee1.getProjects().add(project2);

        this.employeeRepository.save(employee1);


        //Adding test users
        User ramesh = User
                        .builder()
                        .age(22)
                        .email("ramesh@gmail.com")
                        .password("ramesh")
                        .roles(new HashSet<>())
                        .salary(240000)
                        .build();

        User suresh = User
                        .builder()
                        .age(22)
                        .email("suresh@gmail.com")
                        .roles(new HashSet<>())
                        .password("suresh")
                        .salary(240000)
                        .build();


        Role userRole = Role.builder().roleName("ROLE_USER").users(new HashSet<>()).build();
        Role adminRole = Role.builder().roleName("ROLE_ADMIN").users(new HashSet<>()).build();

        Set<Role> roles = new HashSet<>();
        roles.add(userRole);
        roles.add(adminRole);

        ramesh.getRoles().add(userRole);
        ramesh.getRoles().add(adminRole);

        userRole.getUsers().add(ramesh);
        adminRole.getUsers().add(ramesh);


        this.userRepository.save(ramesh);

        System.out.println(" User details :: ");
        ramesh.getRoles().stream().forEach(role -> System.out.println("Roles "+ role));
        System.out.println(" User details :: ");

    }
}