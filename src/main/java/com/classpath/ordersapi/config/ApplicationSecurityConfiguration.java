package com.classpath.ordersapi.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@RequiredArgsConstructor
public class ApplicationSecurityConfiguration { // extends WebSecurityConfigurerAdapter {
/*
    private final UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(userDetailsService)
                .passwordEncoder(paswordEncoder());
    }

    @Override
    //Authorization rules
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().disable();
        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/login/**", "/singup/**", "/contact/**", "/actuator/**", "/h2-console/**", "/h2-console/login.do/**", "/h2-console/login.do?**" )
                    .permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/orders/**")
                    .hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.POST, "/api/v1/orders/**")
                    .hasAnyRole("USER", "ADMIN")
                .anyRequest()
                    .authenticated()
                .and()
                .formLogin()
                    .and()
                .httpBasic();

    }

    @Bean
    public PasswordEncoder paswordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }*/
}