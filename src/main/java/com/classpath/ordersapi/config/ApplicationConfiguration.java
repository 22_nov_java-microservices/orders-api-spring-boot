package com.classpath.ordersapi.config;

import com.classpath.ordersapi.component.Address;
import com.classpath.ordersapi.component.Driver;
import com.classpath.ordersapi.component.RentalCab;
import com.classpath.ordersapi.component.UberGoDriver;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.boot.cloud.CloudPlatform;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@ImportResource("application-context.xml")
@ConditionalOnProperty(name="app.config", havingValue = "true")
public class ApplicationConfiguration {

    @Bean
    @ConditionalOnProperty(name = "app.user", havingValue = "suresh")
    public User user (){
        return new User("Aravind");
    }

    @Bean
    @ConditionalOnBean(name = "user")
    public User userBasedOnBean (){
        return new User("Aravind");
    }

    @Bean
    @ConditionalOnMissingBean(name="driver")
    public Driver uberGo(){
        return new UberGoDriver("Harish", address());
    }

    @Bean
    @ConditionalOnMissingBean(name = "user")
    @ConditionalOnJava(JavaVersion.EIGHT)
    @ConditionalOnCloudPlatform(CloudPlatform.CLOUD_FOUNDRY)
    public User userBasedOnMissingBean (){
        return new User("Aravind");
    }

    @Bean
    @ConditionalOnProperty(name = "app.cabservice", havingValue = "rental" )
    public Driver driver(){
        return new RentalCab("Vinay");
    }



    @Bean
    private Address address() {
        return new Address();
    }

}

class User {

    private String name;
    public User(String name ){
        this.name = name;
    }
}