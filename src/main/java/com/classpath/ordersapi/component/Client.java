package com.classpath.ordersapi.component;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class Client implements CommandLineRunner {

    private ApplicationContext context = null;

    public Client(ApplicationContext applicationContext){
        this.context = applicationContext;
    }

        //Without Spring

       /* Commute fakeDriver = (String from, String to) -> System.out.println("Testing with fake implementation of Commute");


        Passenger passenger = new Passenger(fakeDriver);
        passenger.commute("Koramangala", "Hebbal");*/

        //Using Spring


    @Override
    public void run(String... args) throws Exception {
        final Passenger passenger = (Passenger) context.getBean("passenger");
        passenger.commute("Koramangala", "Airport");
    }
}