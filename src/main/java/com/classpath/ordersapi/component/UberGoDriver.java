package com.classpath.ordersapi.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class UberGoDriver implements Driver {

    private  String name;

    private Address address;

    public UberGoDriver( @Value("Ramesh") String name, Address address){
        this.name = name;
        this.address = address;
    }

    @PostConstruct
    public void init() {
        System.out.println(" After bean has created this method will be called to initialize");
    }

    @PreDestroy
    public void destroy(){
        System.out.println(" Before the bean is destroyed this will be called to clean up the resources");
    }

    public void drive(String from, String to){
        System.out.println(" Driving from "+from + " to "+ to  + " with "+ this.name);
    }
}