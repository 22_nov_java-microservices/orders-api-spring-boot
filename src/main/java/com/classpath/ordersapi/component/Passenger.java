package com.classpath.ordersapi.component;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Passenger {

    private final Driver driver;

    //public Passenger(@Qualifier("uberGoDriver") Driver driver){
    /*public Passenger(Driver driver){
        this.driver = driver;
    }*/
    public void commute(String startLocation, String destination){
        driver.drive(startLocation, destination);
    }
}