package com.classpath.ordersapi.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

//@Component
//@Profile("qa")
public class RentalCab implements Driver {

    private final String name;

    //public RentalCab(@Value("${app.user}") String name){
    public RentalCab(String name){
        this.name = name;
    }
    @Override
    public void drive(String from, String to) {
        System.out.println(" Driving from "+from + " to "+ to  + " with "+ this.name);
    }
}