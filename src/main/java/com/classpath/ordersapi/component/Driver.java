package com.classpath.ordersapi.component;

@FunctionalInterface
public interface Driver {

     void drive(String from, String to);
}