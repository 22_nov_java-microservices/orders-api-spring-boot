package com.classpath.ordersapi.util;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadDemo {

    public static void main(String[] args) throws InterruptedException {
        List<String> data = Arrays.asList("one", "two", "three", "four");

        data.stream().parallel().forEach(data1 -> System.out.println(data1));

        //1. Thread API

        Thread t1 = new Thread(new Job());
        Thread t2 = new Thread(new Job());
        t1.start();
        t2.start();

        t2.join();
        t2.join();
        // 2. Exececutor framework

        final ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(new Job());
        executorService.submit(new Job());


    }
}

class Job implements Runnable {

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Task to be carried in parallel");
    }
}