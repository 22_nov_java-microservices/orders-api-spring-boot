package com.classpath.ordersapi.repository;
import com.classpath.ordersapi.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/*@Repository
public class OrderRepository {
    //specify the data types
    private final static Map<Long, Order> orderMap = new HashMap<>();

    public Order save(Order order) {
        long orderId = Math.round(Math.random() * 25234324234L);
        order.setOrderId(orderId);
        orderMap.put(orderId, order);
        return order;
    }

    public Set<Order> fetchAll() {
        return orderMap
                .values()
                .stream()
                .collect(Collectors.toSet());
    }

    public Optional<Order> findOrderByOrderId(long orderId) {
        return Optional.ofNullable(orderMap.get(orderId));
    }

    public void deleteOrderById(long orderId) {
        orderMap.remove(orderId);
    }
}*/
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findByPriceGreaterThanEqual(double orderPrice);

    Set<Order> findByDateBetween(LocalDate startDate, LocalDate endDate);

    List<Order> findByCustomerNameLike(String name);
}