package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.DomainUserDetails;
import com.classpath.ordersapi.model.User;
import com.classpath.ordersapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DomainUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        System.out.println(" Inside the loadUserByUsername method :: ");
        final User domainUser = this.userRepository
                .findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("Invalid username and/or password"));

        System.out.println(" INside th load UserByUsername method ");
        System.out.println(domainUser.getRoles());
        System.out.println(domainUser.getEmail());
        System.out.println(domainUser.getPassword());
        return new DomainUserDetails(domainUser);
    }
}