package com.classpath.ordersapi.service;

class CurrencyConverter {

    private final EUR eur;

    public CurrencyConverter(EUR eur){
        this.eur = eur;
    }

    public static INR eurToInr(EUR eur) {
        return  new INR(eur.value() * 75);
    }
}


class INR {
    private final double money;

    public INR(double money){
        this.money = money;
    }

    public double value(){
        return this.money;
    }
}

class EUR {
    private final double money;

    public EUR(double money) {
        this.money = money;
    }
    public double value(){
        return this.money;
    }
}

public class CurrencyConverterClient {

    public static void main(String[] args) {
        final INR returnValue = CurrencyConverter.eurToInr(new EUR(55));
        System.out.println(" Currency value in INR "+ returnValue.value());
    }
}