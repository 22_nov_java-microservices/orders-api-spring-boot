package com.classpath.ordersapi.service;


import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

import static org.springframework.data.domain.PageRequest.of;

@Service
@Slf4j
public class OrderService {

    private OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository){
        this.orderRepository = orderRepository;
    }

    public Order saveOrder(Order order){
        //validate the data
        //process the data
        //persist the data to the data store
        return orderRepository.save(order);
    }

    public Map<String, Object> fetchAll(int page, int size, String attribute){
        /*
          Fetch all the records
        Set<Order> orders = new HashSet<>();
        orderRepository.findAll().forEach(orders::add);
        return orders;*/

        final PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, attribute) );
        final Page<Order> response = this.orderRepository.findAll(pageRequest);
        final long totalElements = response.getTotalElements();
        final int totalPages = response.getTotalPages();
        final List<Order> orders = response.getContent();


        log.info(" Records from the query :: "+ orders.size());

        Map<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("pages", totalPages);
        responseMap.put("records", totalElements);
        responseMap.put("data", orders);
        return  responseMap;
    }

    public Set<Order> findAll(){
        Set<Order> orders = new HashSet<>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }

    public Order fetchOrderById(long orderId){
        return orderRepository
                .findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid order id passed"));
    }
    public Order updateOrder(Order order){
        return order;
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }

    public Set<Order> fetchOrdersByDate(LocalDate startDate, LocalDate endDate) {
        return this.orderRepository.findByDateBetween(startDate, endDate);
    }
}