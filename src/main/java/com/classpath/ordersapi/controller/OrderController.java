package com.classpath.ordersapi.controller;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.service.OrderService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/api/v1/orders")
@Slf4j
@OpenAPIDefinition(
        info = @Info(
                title = "Orders API",
                description = "API to access Orders",
                contact = @Contact(
                        name = "developer",
                        email = "developer@gmail.com"
                )
        )
)
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService){
        this.orderService = orderService;
    }

    /*
      1. GET
           - Querying the data from the server
           - Does not cause side affects
           - Can be bookmarked
           - The parameters are sent as part of the url
           - Cannot be used to send sensitive information
      2. POST
           - Not Idempotent
           - Does cause the side affect
           - Cannot be bookmark
           - Data is sent in the body and hence can be used to send sensitive information
           - Form submission, Uploading documents, Authentication form, sending sensitive information
      3. PUT
          - Update an existing resource
          - Does not cause a side affect
          - Data is updated

      4. DELETE
          - Delete the resource
          - Does not cause a side affect
     */

     @PostMapping
     @ResponseStatus(CREATED)
     @ApiResponses({
             @ApiResponse(description = "Succesfully saved the order", responseCode = "201" ),
             @ApiResponse(description = "Invalid Order format", responseCode = "400" ),
             @ApiResponse(description = "UnAuthorized access", responseCode = "401" )
     })
      public Order saveOrder(@Parameter(name="order", required = true) @RequestBody @Valid Order order){
         log.info(" Saving the orders ::  {} ", order);
         //order.getLineItems().forEach(lineItem -> lineItem.setOrder(order));
          return this.orderService.saveOrder(order);
      }

      @GetMapping
      public Map<String, Object> fetchAllOrders(
              @RequestParam(required = false, defaultValue = "1",name = "page") int page,
              @RequestParam(required = false, defaultValue = "20",name = "size") int size,
              @RequestParam(required = false, defaultValue = "price",name = "sort") String sort
              ){

        return this.orderService.fetchAll(page, size, sort);
      }

      @GetMapping("/{id}")
      public Order findOrderByOrderId(@PathVariable("id") long orderId){
          return this.orderService.fetchOrderById(orderId);
      }

      @PutMapping("/{id}")
      public Order updateOrder(@PathVariable("id") long orderId, @RequestBody Order order){
          return null;
      }

      //short hand syntax
      @DeleteMapping("/{id}")
      @ResponseStatus(NO_CONTENT)
      public void deleteOrderbyOrderId(@PathVariable long id){
            this.orderService.deleteOrderById(id);
      }

      @GetMapping("/{startdate}/{enddate}")
      public Set<Order> fetchOrdersByDate(
              @DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable("startdate") LocalDate startDate,
              @DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable("enddate") LocalDate endDate){
          return this.orderService.fetchOrdersByDate(startDate, endDate);
      }
}