package com.classpath.ordersapi.controller;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.service.OrderService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

//@Controller
public class OrderClient  { //implements CommandLineRunner {

    private final ApplicationContext applicationContext;
    private final OrderService orderService;

    public OrderClient(ApplicationContext applicationContext, OrderService orderService){
        this.applicationContext = applicationContext;
        this.orderService = orderService;
    }

    //@Override
    public void run(String... args) throws Exception {
        //accept the request and send the response
        try (Scanner scanner = new Scanner(System.in)) {

            System.out.println("Please enter your option ");
            System.out.println("1 : Create order");
            System.out.println("2 : List all orders");
            System.out.println("3 : Fetch order by orderId");
            System.out.println("4 : Update order by orderId");
            System.out.println("5 : Delete order by orderId");

            int option = scanner.nextInt();

            switch (option){
                case 1:
                    saveOrder();
                    break;
                case 2:
                    listOrders();
                    break;
                case 3:
                    fetchOrderById();
                    break;
                case 4:
                    updateOrderById();
                    break;
                case 5:
                    deleteOrderById();
                    break;
                default:
                    listOrders();
            }
        } catch (Exception exception){
            System.out.println(" Exception :: "+ exception.getMessage());
        }
    }

    private  void saveOrder() {
        Order order1 = new Order();
        order1.setCustomerName("Vinod");
        order1.setDate(LocalDate.now());
        order1.setCustomerEmail("vinod@gmail.com");
        order1.setPrice(15000);

        //Order o = new Order("Ram", "Anand" , 22, 25, true, true, false);
        Order order2 = Order.builder().customerName("Vinod").customerEmail("vinod@gmail.com").date(LocalDate.now()).price(12_000).build();
        orderService.saveOrder(order1);
        orderService.saveOrder(order2);
        listOrders();
    }

    private  void listOrders() {
        final Set<Order> orders = new HashSet<>();
        orders.stream().forEach(System.out::println);
    }
    private  void fetchOrderById() {
        orderService.fetchOrderById(12L);
    }
    private  void updateOrderById() {
        orderService.updateOrder(null);
    }
    private void deleteOrderById() {
        orderService.deleteOrderById(12L);
    }


}