package com.classpath.ordersapi.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(NOT_FOUND)
    public Error handleInvalidOrderId(IllegalArgumentException exception){
        log.info(" Exception while fetching Order by order id "+ exception.getMessage());
        return new Error(100, exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public Map<String, List<String>> handleInvalidOrder(MethodArgumentNotValidException exception){
        log.error(" Exception while accepting input : {}", exception.getMessage());
        final List<ObjectError> allErrors = exception.getAllErrors();
        final List<String> errors = allErrors.stream()
                                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
                                        .collect(Collectors.toList());
        Map<String,List<String>> errorMap = new HashMap<>();
        errorMap.put("errors", errors);
        return errorMap;
    }
}

@AllArgsConstructor
@Data
class Error {
    private final int code;
    private final String message;
}