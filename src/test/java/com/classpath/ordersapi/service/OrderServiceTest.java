package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    @Test
    public void testFetchAllOrders(){
        // 1. set the expectation - Optional (in case of mock objects)
        //2. setup - optional
        //3. execute - mandatory
        //4. assertions - mandatory
        //5. verify the expecations set - Optional (in case of mock objects)

        when(orderRepository.findAll()).thenReturn(asList(Order
                .builder()
                .orderId(12L)
                .price(22000)
                .date(LocalDate.now())
                .customerName("vinod")
                .customerEmail("vinod@gmail.com")
                .build()));
        final Set<Order> fetchedOrders = orderService.findAll();

        Assertions.assertNotNull(fetchedOrders);
        Assertions.assertEquals(1, fetchedOrders.size());

        verify(orderRepository, times(1)).findAll();
    }

    @Test
    public void testFetchOrderUsingInvalidOrderId(){

        when(orderRepository.findById(anyLong())).thenReturn(Optional.empty());


        try {
            orderService.fetchOrderById(12L);
            fail("This test case should throw InvalidArguement Exception");
        } catch (IllegalArgumentException exception){
            assertNotNull(exception);
            assertEquals("Invalid order id passed", exception.getMessage());
        }

        verify(orderRepository, times(1)).findById(12L);
    }

    @Test
    public void testInvalidOrderId(){
        when(orderRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(IllegalArgumentException.class, () -> orderService.fetchOrderById(12L));
    }

    @Test
    public void testFetchOrderUsingValidOrderId(){

        when(orderRepository.findById(anyLong())).thenReturn(of(Order
                .builder()
                .orderId(12L)
                .price(22000)
                .date(LocalDate.now())
                .customerName("vinod")
                .customerEmail("vinod@gmail.com")
                .build()));
        try {
            Order fetchedOrder = orderService.fetchOrderById(12L);
            assertNotNull(fetchedOrder);
        } catch (Exception exception){
            fail("This test case should not throw  Exception");
        }
        verify(orderRepository, times(1)).findById(12L);
    }

    @Test
    public void testValidOrderId(){
        when(orderRepository.findById(anyLong())).thenReturn(of(Order
                .builder()
                .orderId(12L)
                .price(22000)
                .date(LocalDate.now())
                .customerName("vinod")
                .customerEmail("vinod@gmail.com")
                .build()));
        assertDoesNotThrow(() -> orderService.fetchOrderById(12L), "This method should not throw exception");
    }

    @Test
    public void testDeleteOrder(){

        //no expectations on the mock

        orderService.deleteOrderById(12L);

        //verification
        verify(orderRepository, times(1)).deleteById(12L);
    }

    @Test
    public void testSaveOrder(){

        Order order = Order
                .builder()
                .orderId(12L)
                .price(22000)
                .date(LocalDate.now())
                .customerName("vinod")
                .customerEmail("vinod@gmail.com")
                .build();

        //set the expectations
        when(orderRepository.save(any(Order.class))).thenReturn(order);

        final Order savedOrder = orderService.saveOrder(new Order());

        Assertions.assertNotNull(savedOrder);
        Assertions.assertEquals(12L, savedOrder.getOrderId());

        //verification
        verify(orderRepository, times(1)).save(any(Order.class));
    }

    @Test
    public void testInvalidFetchAllOrdersByPagination(){
        when(orderRepository.findAll(any(PageRequest.class))).thenReturn(new Page<Order>() {
            @Override
            public int getTotalPages() {
                return 0;
            }

            @Override
            public long getTotalElements() {
                return 0;
            }

            @Override
            public <U> Page<U> map(Function<? super Order, ? extends U> converter) {
                return null;
            }

            @Override
            public int getNumber() {
                return 0;
            }

            @Override
            public int getSize() {
                return 0;
            }

            @Override
            public int getNumberOfElements() {
                return 0;
            }

            @Override
            public List<Order> getContent() {
                return new ArrayList<>();
            }

            @Override
            public boolean hasContent() {
                return false;
            }

            @Override
            public Sort getSort() {
                return null;
            }

            @Override
            public boolean isFirst() {
                return false;
            }

            @Override
            public boolean isLast() {
                return false;
            }

            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public Pageable nextPageable() {
                return null;
            }

            @Override
            public Pageable previousPageable() {
                return null;
            }

            @Override
            public Iterator<Order> iterator() {
                return null;
            }
        });

        final Map<String, Object> fetchedOrders = orderService.fetchAll(1, 20, "price");
        Assertions.assertNotNull(fetchedOrders);

        verify(orderRepository, times(1)).findAll(PageRequest.of(1, 20, Sort.by(Sort.Direction.ASC, "price")));
    }

}