package com.classpath.ordersapi.model;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class OrderTest {

    private static Order order;

    @BeforeAll
    public static void setUp(){
        order = Order
                    .builder()
                    .orderId(12L)
                    .price(22000)
                    .date(LocalDate.now())
                    .customerName("vinod")
                    .customerEmail("vinod@gmail.com")
                .build();
    }

    @Test
    public void testOrderConstructor(){
        // 1. set the expectation - Optional (in case of mock objects)
        //2. setup - optional
        //3. execute - mandatory
        Order order = new Order();
        //4. assertions - mandatory
        assertNotNull(order);
        //5. verify the expecations set - Optional (in case of mock objects)
    }
    @Test
    public void testOrderUsingBuilder(){
        // 1. set the expectation - Optional (in case of mock objects)
        //2. setup - optional
        //3. execute - mandatory
        //4. assertions - mandatory
        assertNotNull(order);
        assertEquals(12L, order.getOrderId());
        assertEquals(22000, order.getPrice());
        assertEquals(LocalDate.now(), order.getDate());
        assertEquals("vinod", order.getCustomerName());
        assertEquals("vinod@gmail.com", order.getCustomerEmail());
        //5. verify the expecations set - Optional (in case of mock objects)
    }

    @Test
    public void testSetters(){
        order.setOrderId(1234);
        order.setCustomerEmail("hari@gmail.com");
        order.setCustomerName("Hari");
        order.setPrice(45000);

        //assertions
        assertEquals(1234, order.getOrderId());
        assertEquals("hari@gmail.com", order.getCustomerEmail());
        assertEquals("Hari", order.getCustomerName());
        assertEquals(45000, order.getPrice());
    }

    @AfterAll
    public static void tearDown(){
        order = null;
    }
}